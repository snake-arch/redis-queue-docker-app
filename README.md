# Basic Queue Implementation 
Used redis, docker for containerization and flask as backend.

# Steps to run
* cd Redis-App/Front-end-Routing/
* run "sudo docker build redisapp:latest ."
* run "sudo docker run -it --network="host" redisapp"

# Other commands
* sudo docker ps
* sudo docker rmi 'id'
