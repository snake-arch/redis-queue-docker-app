FROM ubuntu:20.04
RUN apt-get update 
RUN apt-get upgrade -y
RUN apt-get install -y redis
RUN apt-get install -y python3-dev 
RUN apt-get install -y python3-pip

WORKDIR /app

COPY . /app

RUN pip3 install --no-cache-dir --upgrade pip
RUN pip3 install --no-cache-dir redis flask

EXPOSE 5000
ENTRYPOINT ["python3"]
CMD ["Front-end-Routing.py"]

