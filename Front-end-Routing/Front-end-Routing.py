from flask import Flask,redirect, url_for, request, flash, render_template
import redis
client = redis.StrictRedis(host='localhost',port=6379)

app = Flask(__name__,template_folder='templates')


# ‘/’ URL is bound with hello_world() function.
@app.route('/')
def front_page():
    return render_template('Front-end.html')

@app.route('/post',methods = ['POST','GET'])
def push_value():
   if request.method == 'POST':
      number = str(request.form['nm'])
      print(number)
      if len(number)!=0:
          try:
            client.rpush("Queue",number)
            flash("Successfully pushed {}".format(number))
            #return redirect('http://localhost:63342/redis-stuff/Front-end.html')
            return render_template('Front-end.html')

          except Exception as e: 
            print(e)
            flash("Some error occured")
            #return redirect('http://localhost:63342/redis-stuff/Front-end.html')
            return render_template('Front-end.html')

      else:
          print('else')
          flash("Input cannot be empty")
          #return redirect('http://localhost:63342/redis-stuff/Front-end.html')
          return render_template('Front-end.html')
   else:
      return render_template('Front-end.html')

@app.route('/get', methods=['GET'])
def pop_value():
    if request.method == 'GET':
        try:
            number = client.lpop("Queue").decode('utf-8')
            flash(number)
            return render_template('Front-end2.html')
            #return redirect('http://localhost:63342/redis-stuff/Front-end.html')

        except:
            flash("No more elements")
            return render_template('Front-end2.html')
            #return redirect('http://localhost:63342/redis-stuff/Front-end.html')
    else:
        return render_template('Front-end.html')


    #finally:
    #    flash("Some error occured")
    #    return render_template('Front-end2.html')


# @app.route('/author')
# def author():
#    return 'Author : Gaurav Maindola'
# #app.add_url_rule('/author','g2g',author)
#
# @app.route('/hello/<name>')
# def hello_name(name):
#    return 'Hello %s!' % name

# main driver function
if __name__ == '__main__':
    # run() method of Flask class runs the application
    app.debug = True
    # on the local development server.
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'redis'
    app.config['SESSION_PERMANENT'] = False
    app.config['SESSION_USE_SIGNER'] = True
    app.config['SESSION_REDIS'] = redis.from_url('redis://localhost:6379')
    app.run(host='0.0.0.0',port = 5050)
